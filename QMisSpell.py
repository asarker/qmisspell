"""
LAST CODE UPDATE: 3/30/2018
MISSPELLING GENERATION SCRIPT FOR THE PAPER:

SARKER A, GONZALEZ G. AN UNSUPERVISED AND CUSTOMIZABLE MISSPELLING GENERATOR FOR MINING NOISY HEALTH-RELATED TEXT SOURCES. [UNDER PEER-REVIEW]

ASSOCIATED PUBLICATION CURRENTLY UNDER PEER-REVIEW; PREPRINT AVAILABLE AT ARXIV

Further details will be provided soon. Email questions to: abeed@pennmedicine.upenn.edu

"""


from string import strip
from collections import defaultdict
from gensim.models import Word2Vec, KeyedVectors
import Levenshtein, re
import sys



def generate_spelling_variants(seedwordlist, word_vectors, semantic_search_length=500, levenshtein_threshold = 0.85, setting = 1):
    """
        setting -> 0 = weighted levenshtein ratios
                -> 1 = standard levenshtein ratios

    :param seedwordlist:            list of words for which spelling variants are to be generated
    :param word_vectors:            the word vector model
    :param semantic_search_length:  the number of semantically similar terms to include in each iteration
    :param levenshtein_threshold:   the threshold for levenshtein ratio

    :return: dictionary containing the seedwords as key and all the variants as a list of values

    """
    vars = defaultdict(list)
    for seedword in seedwordlist:
        #a dictionary to hold all the variants, key: the seedword, value: the list of possible misspellings
        #a dynamic list of terms that are still to be expanded
        terms_to_expand = []
        terms_to_expand.append(seedword)
        all_expanded_terms = []
        level = 1
        while len(terms_to_expand)>0:
                t = terms_to_expand.pop(0)
                all_expanded_terms.append(t)
                try:
                    similars = word_vectors.most_similar(t, topn=semantic_search_length)
                    for similar in similars:
                        similar_term = similar[0]
                        if setting == 1:
                            seq_score = Levenshtein.ratio(str(similar_term),seedword)
                        if setting == 0:
                            seq_score = weighted_levenshtein_ratio(str(similar_term), seedword)
                        if seq_score>levenshtein_threshold:
                            if not re.search(r'\_',similar_term):
                                vars[seedword].append(similar_term)
                                if not similar_term in all_expanded_terms and not similar_term in terms_to_expand:
                                    terms_to_expand.append(similar_term)
                except:
                        pass
                level+=1
        vars[seedword] = list(set(vars[seedword]))
    return vars

def weighted_levenshtein_ratio(variant,seedword):
    """
    Given a possible variant and a seedword, returns the weighted average levenshtein ratio for the pair
    :param variant:
    :param seedword:
    :return:
    """
    l_ratio_vals = []
    window_beg = 0
    window_end = int((len(seedword)-1)/2)
    if len(variant)<len(seedword):
        variant = pad(variant,len(seedword))
    elif len(seedword)<len(variant):
        seedword = pad(seedword,len(variant))

    while window_end <= len(seedword) and window_end <= len(variant):
        relpos = ((window_beg + window_end+0.)/2.) / len(seedword)
        if relpos < 0.2:
            relpos = 0.1
        elif relpos < 0.4:
            relpos = 0.3
        elif relpos < 0.6:
            relpos = 0.5
        elif relpos < 0.8:
            relpos = 0.7
        elif relpos < 1.0:
            relpos = 0.9
        levenshtein_ratio = Levenshtein.ratio(seedword[window_beg:window_end], variant[window_beg:window_end])
        levenshtein_ratio_weighted = weights[relpos] * levenshtein_ratio
        l_ratio_vals.append(levenshtein_ratio_weighted)

        window_beg += 1
        window_end += 1
    average_weighted_levenshtein_ratio = sum(l_ratio_vals)/len(l_ratio_vals)
    return average_weighted_levenshtein_ratio

def pad(drugname, required_length):
    """
    Helper function for padding terms when computing weighted Levenshtein Ratio
    """
    post_padding = ''
    for i in range(0,required_length):
        if i<len(drugname):
            post_padding+=drugname[i]
        else:
            post_padding+='?'
    return post_padding



def loadVectorModel(filename):
    """
        Loads and returns a word2vec vector model
        :param filename: the filename for the model
        :return: the loaded model
    """
    print 'loading model this may take a while'
    word_vectors = KeyedVectors.load_word2vec_format(filename, binary=True, encoding='utf8', unicode_errors='ignore')
    return word_vectors

def loadSeedWordsFromFile(filename):
    """
        Given a filename that contains a set of words (line separated), loads and returns them..

    :param filename: the filename containing the words
    :return: a list of words
    """
    seedwordlist = []
    word_vector_model = ''
    infile = open(filename)
    for line in infile:
        seedwordlist.append(strip(line).lower())
    return seedwordlist

if __name__ =='__main__':

    seedwordlist = []
    vector_model_filename = ''
    try:
        f = sys.argv[1]
        vector_model_filename = sys.argv[2]

        # LOAD THE LIST OF SEED WORDS
        seedwordlist = loadSeedWordsFromFile('./'+ f)
        # LOAD THE VECTOR MODEL
        word_vectors = loadVectorModel(vector_model_filename)
    except IndexError:
        print 'Incorrect arguments specified (may be you didn\'t specify any arguments..'
        print 'Format: python [scriptname] [inputfilename] [path_to_word_vector_model]'

    # THE WEIGHTS HAVE BEEN PRECOMPUTED
    weights = {0.5: 1.0133116199788939, 0.7: 0.97859255044477178, 0.9: 0.94999999999999996, 0.3: 1.05, 0.1: 0.99601150272112082}


    #SET THE LEVENSHTEIN RATIO THRESHOLD
    lt  = 0.7 #lower this to increase recall and decrease precision

    #SET THE NUMBER OF SEMANTICALLY SIMILAR TERMS TO INCLUDE IN EACH ITERATION
    ssl = 4000

    #DEFAULT SETTING
    spelling_variants_default = generate_spelling_variants(seedwordlist, word_vectors, semantic_search_length=ssl, levenshtein_threshold=lt, setting=1)

    #WEIGHTED LEVENSHTEIN RATIO
    spelling_variants_weighted = generate_spelling_variants(seedwordlist, word_vectors, semantic_search_length=ssl, levenshtein_threshold=lt, setting=0)

    for k,v in spelling_variants_weighted.items():
        print k,'\t',' '.join(v)
    print '----------'
    for k,v in spelling_variants_default.items():
        print k, '\t', ' '.join(v)
    print '----------'

print
print 'Done...'
