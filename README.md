#REQUIREMENTS#

python 2.7.*

Libraries:

gensim

Levenshtein


##TO RUN:##

python scriptname inputfilename path_to_word_vector_model


*e.g.:*
python QMisSpell.py seedwords /Username/Documents/trig-vectors-phrase.bin



Sample outputs are provided in the following files:

sample_misspellings_setting_0

sample_misspellings_setting_1


NOTE: Any word2vec model can be used. The experiments were conducted using the publicly available model at: https://data.mendeley.com/datasets/dwr4xn8kcv/3

Email questions to: abeed@pennmedicine.upenn.edu
